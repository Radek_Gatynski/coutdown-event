let createEvent = document.getElementById('createEvent');
let createEventName = document.getElementById('createEventName');
let createEventDate = document.getElementById('createEventDate');


let loadEvent = document.getElementById('loadEvent');
let loacEventName = document.getElementById('loacEventName');

let formsEventGETorPOST = document.querySelector('.formsEventGETorPOST');
let showEventCount = document.querySelector('.showEventCount');

let countDownDate = new Date("Feb 20, 2019");

const ourApiUrlGET = "http://localhost/php/getEvent.php";
const ourApiUrlPUT = "http://localhost/php/putEvent.php";
let responseVal = "";

function createNewDate(d) {
  let arr = d.split("-");
  let year = arr[0];
  let month = arr[1]-1;
  let day = arr[2];
  arr = new Date(year, month, day);
  return arr;
}
function main(){
createEvent.addEventListener('click', function(){
  createEventName = createEventName.value;
  if(createEventName.includes("?")){
    let info = document.getElementById("smallInfoCreate");
    info.classList.remove('hidden');
    info.innerText = "The event name can't contain '?' please reload page";
    return false;
  }
  createEventDate = createEventDate.value;
  console.log(createEventName + " " + createEventDate);
  countDownDate = createNewDate(createEventDate);
  countDownDate = countDownDate.getTime();
  if(createEventName && createEventDate !='Invalid Date' && countDownDate){  
    putToDB(createEventName, createEventDate);
  }
  else{
    return false;
  }
})

loadEvent.addEventListener('click', function(){
  loacEventName = loacEventName.value;
  console.log(loacEventName);
  if(loacEventName){   
     getFromDB(loacEventName);
  }
  else{
    return false;
  }
})
}
main();
function getFromDB(loacEventName){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        FieldFromDB(this.responseText);
      }
  };
  xmlhttp.open("GET", ourApiUrlGET+"?eventName="+loacEventName);
  xmlhttp.send();
}

function putToDB(createEventName, createEventDate){
  var xmlhttp = new XMLHttpRequest();
  xmlhttp.onreadystatechange = function() {
      if (this.readyState == 4 && this.status == 200) {
        FieldPutDB(this.responseText);
      }
  };
  xmlhttp.open("POST", ourApiUrlPUT+"?eventData="+createEventName+"?"+createEventDate, true);
  xmlhttp.send();
}


function FieldFromDB(dateEvent){
  if(dateEvent != 'false'){
    formsEventGETorPOST.parentNode.removeChild(formsEventGETorPOST);
    showEventCount.classList.remove('hidden');
    eventName.innerText = loacEventName;
    let newDate = createNewDate(dateEvent);
    countDownDate = newDate.getTime();
    setInterval(countDown, 1000);
  }
  else{
    console.log(dateEvent);
    let info = document.getElementById("smallInfoGET");
    info.classList.remove('hidden');
    info.innerText = "The event don't exist in DB, please reload page";
    return false;
  }
}

function FieldPutDB(wynik){
  console.log(wynik);
  if(wynik == 'inserted'){
    formsEventGETorPOST.parentNode.removeChild(formsEventGETorPOST);
    showEventCount.classList.remove('hidden');
    eventName.innerText = createEventName;
    setInterval(countDown, 1000);
  }
  else{
    let info = document.getElementById("smallInfoCreate");
    info.classList.remove('hidden');
    info.innerText = "The event with this name exist in DB, please reload page";
    return false;
  }
}



function countDown(){
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
    document.getElementById('days').innerHTML = days;
    document.getElementById('hours').innerHTML = hours;
    document.getElementById('minutes').innerHTML = minutes;
    document.getElementById('secundes').innerHTML = seconds;
    if (distance < 0) {
      document.getElementById('counter-downEX').innerHTML = "EXPIRED";
    }
}
