let countUntil = document.getElementById('countUntil').textContent;

console.log(countUntil);
let countDownDate = new Date("Feb 20, 2019");

function createNewDate(d) {
  let arr = d.split("-");
  let year = arr[0];
  let month = arr[1]-1;
  let day = arr[2];
  arr = new Date(year, month, day);
  return arr;
}

let newDate = createNewDate(countUntil);
countDownDate = newDate.getTime();
setInterval(countDown, 1000);
function countDown(){
    var now = new Date().getTime();
    var distance = countDownDate - now;
    var days = Math.floor(distance / (1000 * 60 * 60 * 24));
    var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconds = Math.floor((distance % (1000 * 60)) / 1000);
  
    document.getElementById('days').innerHTML = days;
    document.getElementById('hours').innerHTML = hours;
    document.getElementById('minutes').innerHTML = minutes;
    document.getElementById('secundes').innerHTML = seconds;
    if (distance < 0) {
      document.getElementById('counter-downEX').innerHTML = "EXPIRED";
    }
}