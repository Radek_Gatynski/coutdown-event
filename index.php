<?php
include './php/Config.php';
$connect = new Config();
include './php/Event.php';
$event = new Event($connect);
include './php/ErrorsMessages.php';
include './handles/newEvent.php';
include './handles/loadEvent.php';
include './header.php';
?>
<h5 class="text-center"><?php echo $event->getError(ErrorsMessages::$newEvent); ?></h5>
<div class="container">
    <div class="row">
        <div class="col-lg-6 col-xs-12">
        <header><h2>Create event</h2></header>
            <form class="form text-center" method="POST" action="index.php">
                <div class="form-group">
                    <label for="eventName">Event name</label>
                    <input class="form-control" type="text" name="name" placeholder="Event name" required id="eventName" />
                    <?php echo $event->getError(ErrorsMessages::$emptyName); ?>
                    <?php echo $event->getError(ErrorsMessages::$nameLength); ?>
                    <?php echo $event->getError(ErrorsMessages::$eventNameExist); ?>
                </div>
                <div class="form-group">
                    <label for="eventDate">Date</label>
                    <input type="date" class="form-control" name="date" placeholder="Event date" required id="eventDate" />
                </div>
                <input type="submit" name="newEvent" class="btn btn-primary" />
            </form>
        </div>

        <div class="col-lg-6 col-xs-12">
            <header><h2>Load event</h2></header>
            <form class="form text-center" method="POST" action="index.php">
                <div class="form-group">
                    <label for="eventName">Event name</label>
                    <input class="form-control" type="text" name="eventName" placeholder="Event name" required id="eventName" />
                    <?php echo $event->getError(ErrorsMessages::$emptyName); ?>
                    <?php echo $event->getError(ErrorsMessages::$nameLength); ?>
                    <?php echo $event->getError(ErrorsMessages::$eventNameDontExist); ?>
                </div>
                <input type="submit" name="loadEvent" class="btn btn-primary" />
            </form>

        </div>

    </div>
</div>


<?php include './footer.php'; ?>