<?php 
class Config{
    private $hostname = 'localhost';
    private $username = 'root';
    private  $password = '';
    private $db = 'eventClock';
    private $pdo;

    public function connect(){
        try{
            $this->pdo = new PDO("mysql:host=" . $this->hostname . ";dbname=" . $this->db, $this->username, $this->password);
            $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
           return $this->pdo;
        } catch(PDOException $e){
            die("ERROR: Could not connect. " . $e->getMessage());
        }
    }
}
?>