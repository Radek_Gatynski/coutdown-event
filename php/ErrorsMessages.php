<?php 
    class ErrorsMessages{
        public static $emptyName = "Event name is empty"; 
        public static $nameLength = "Event name should have min 5 characters and max 30 characters"; 
        public static $eventNameExist = "This event name exist in database";
        public static $dateFormatIsWrong = "The date format is wrong Y-M-D";
        public static $newEvent = "New event inserted to database";
        public static $failInsert = "Fail when inserted new evenet to database";
        public static $eventNameDontExist = "We don't have this event in database";
    }
?>