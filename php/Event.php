<?php 
//include '../php/Config.php';

    class Event{
        private $errors;
        private $pdo;
        public function __construct($connect)
        {
 
            $this->pdo = $connect->connect();
            $this->errors = array();
        }
        public function newEvent($name,$date){
            $this->validateName($name);
            $this->validateDate($date);
            if(empty($this->errors)){
                $result = $this->insertNewEvent($name,$date);
                return $result;
            }else{
                return false;
            }
        }
        public function loadEvent($name){
            $this->validateNameNotExist($name);
            print_r($this->errors);
            if(empty($this->errors)){
                return $this->getEventInfo($name);
            }else{
                return false;
            }
            
        }
        private function getEventInfo($name){
            $sql = "SELECT * FROM event where eventName=?";
            if($stmt=$this->pdo->prepare($sql)){
                $stmt->bindParam(1,$name);
                if($stmt->execute()){
                    if($stmt->rowCount()==1){
                        $result = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        unset($stmt);
                        return $result;
                    }else{
                        unset($stmt);
                        return false;
                    }
                }else{
                    return false;
                }
            }
        }
        private function validateNameNotExist($name){
            $name = trim($name);
            if(empty($name)){
                array_push($this->errors, ErrorsMessages::$emptyName);
            }elseif(strlen($name)<5 || strlen($name)>30){
                array_push($this->errors,ErrorsMessages::$nameLength);
            }elseif(strlen($name)>5 || strlen($name)<30){
               $test = $this->checkNameIfNotExist($name);
               if(!$test){
                  array_push($this->errors,ErrorsMessages::$eventNameDontExist);
               }
            }else{
                return true;
            }
        }
        private function checkNameIfNotExist($name){
            $sql = "Select eventName from event where eventName = ?";
            if($stmt = $this->pdo->prepare($sql)){
              $stmt->bindParam(1, $name);
              if($stmt->execute()){
                  if($stmt->rowCount() == 1){
                      unset($stmt);
                      return true;
                  }
                  unset($stmt);
                  return false;
              }
            }
          }


        private function validateName($name){
            $name = trim($name);
            if(empty($name)){
                array_push($this->errors, ErrorsMessages::$emptyName);
            }elseif(strlen($name)<5 || strlen($name)>30){
                array_push($this->errors,ErrorsMessages::$nameLength);
            }elseif(strlen($name)>5 || strlen($name)<30){
               $test = $this->checkNameIfExist($name);
               if($test){
                  array_push($this->errors,ErrorsMessages::$eventNameExist);
               }
            }else{
                return true;
            }
        }

        private function checkNameIfExist($name){
          $sql = "Select eventName from event where eventName = ?";
          if($stmt = $this->pdo->prepare($sql)){
            $stmt->bindParam(1, $name);
            if($stmt->execute()){
                if($stmt->rowCount()==1){
                    unset($stmt);
                    return true;
                }
                unset($stmt);
                return false;
            }
          }
        }
        private function validateDate($date,$format = 'Y-m-d'){
            $d = DateTime::createFromFormat($format, $date);
            $result = $d && $d->format($format) === $date;
            if($result){
                return true;
            }else{
                array_push($this->errors,ErrorsMessages::$dateFormatIsWrong);
            }
        }
        private function insertNewEvent($name,$date){
            $sql = "INSERT INTO EVENT (eventName, eventDate) VALUES (?,?)";
            if($stmt = $this->pdo->prepare($sql)){
               $stmt->bindParam(1,$name);
               $stmt->bindParam(2,$date);
                if($stmt->execute()){
                    $count = $stmt->rowCount();
                    if($count==1){
                        unset($stmt);
                        array_push($this->errors,ErrorsMessages::$newEvent);
                        return true;
                    }else{
                        unset($stmt);
                        array_push($this->errors,ErrorsMessages::$failInsert);
                        return false;
                    }
                }
                else{
                    array_push($this->errors,ErrorsMessages::$failInsert);
                    unset($stmt);
                    return false;
                }
            }
        }
        public function getError($msg_error){
            if(!in_array($msg_error, $this->errors)){
                $msg_error = "";
            }
            return "<small class='form-text text-muted'>$msg_error</small>";
        }
    }
?>